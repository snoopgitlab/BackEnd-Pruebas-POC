let mercadopago = require('mercadopago');

// token privado de vendedor (cuenta de prueba)

VENDEDOR_TOKEN = "TEST-7692193408177072-090717-4b34e69807b20c4ceedb16a94a5a9169-639873581"
GONZA_TOKEN = "TEST-7149281287981356-080319-b71d8cde72863bd5cb5ffcffc92cd9a6-391087013"

mercadopago.configurations.setAccessToken(VENDEDOR_TOKEN);

const obtenerCliente = async (email) => {
    let filters = { email }
    try{
        let query = await mercadopago.customers.search({
            qs: filters
        })    
        console.log(query)
        let response
        query.body.results.length == 0 ? response = {} : response = query.body.results[0]
        return response
    }
    catch(err){
        throw new Error(err.message)
    }
}

const crearCliente = async (email) => {

    const customer_data = { "email": email }
    try{
        customer = await mercadopago.customers.create(customer_data)
        console.log("Cliente creado satisfactoriamente")
        console.log(customer)
        return customer
    }
    catch (err){
        console.log(err)
        throw new Error(err.message)
    }
}

const listarClientes = async () => {
    try{
        let query = await mercadopago.customers.search({})    
        console.log(query)
        let response
        query.body.results.length == 0 ? response = [] : response = query.body.results
        return response
    }
    catch(err){
        throw new Error(err.message)
    }
}
module.exports = {
    obtenerCliente,
    crearCliente,
    listarClientes
}