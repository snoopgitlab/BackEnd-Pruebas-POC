let mercadopago = require('mercadopago');

// token privado de vendedor (cuenta de prueba)
mercadopago.configurations.setAccessToken("TEST-7692193408177072-090717-4b34e69807b20c4ceedb16a94a5a9169-639873581");

const pagar = async (data) => {
    /*    Los campos mínimos requeridos a enviar son: 
     token, transaction_amount, installments, payment_method_id y el payer.email.*/
    let payment_data = {
        transaction_amount: Number(data.transactionAmount), // Monto a pagar
        token: data.token, // Token de la tarjeta de crédito/débito
        description: data.description, // Descripción del producto/pago
        installments: Number(data.installments), // CUOTAS
        payment_method_id: data.paymentMethodId, // Método de pago, viene en el form
        issuer_id: data.issuer,
        payer: { // Datos del comprador
            email: data.email, // Correo
            identification: { 
                type: data.docType, // Tipo de documento
                number: data.docNumber // Número de documento
            }
        }
    }
    console.log(payment_data)

    try{
        let response = await mercadopago.payment.save(payment_data)
        console.log(response)
        return {
            status: response.status,
            data: response.body
        }
        
    }
    catch(err) {
        console.log("No se pudo realizar el pago correctamente")
        return {
            status: response.status,
            data: error
        }
    }
}

module.exports = {
    pagar
}