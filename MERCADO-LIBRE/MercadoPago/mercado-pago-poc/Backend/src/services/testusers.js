const axios = require('axios')

// Privada para crear usuarios (Cuenta de Gonza)
const ACCESS_TOKEN = "TEST-7149281287981356-080319-b71d8cde72863bd5cb5ffcffc92cd9a6-391087013"

const crearTestUser = async () => {
    let response 
    const options = {
        method: 'POST',
        url: `https://api.mercadopago.com/users/test_user?access_token=${ACCESS_TOKEN}`,
        data:{
            "site_id":"MLA"
        }
    }
    
    try{
        response = await axios(options)
        return response.data
    }
    catch(err){
        console.log(err)
        throw new Error(err.message)
    }
}

module.exports = {
    crearTestUser
}
    