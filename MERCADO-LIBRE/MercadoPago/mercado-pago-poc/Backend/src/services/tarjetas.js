let mercadopago = require('mercadopago');
const clientesService = require('../services/clientes')

// token privado de vendedor (cuenta de prueba)
mercadopago.configurations.setAccessToken("TEST-7692193408177072-090717-4b34e69807b20c4ceedb16a94a5a9169-639873581");

const guardarTarjeta = async (token, email) => {
    try{
        let cliente = await clientesService.obtenerCliente(email)
        let card_data = {
            "token": token,
            "customer_id": cliente.id
        }
        
        let card = await mercadopago.card.create(card_data)
        console.log(card)
        return card
    }
    catch(err){
        console.log(err)
        throw new Error(err.message)
    }
}

const listarTarjetas = async (email) => {
    
    try{
        let cliente = await clientesService.obtenerCliente(email)
        return cliente.cards
    }
    catch(err){
        console.log(err)
        throw new Error(err.message)
    }
}

module.exports = {
    guardarTarjeta,
    listarTarjetas
}