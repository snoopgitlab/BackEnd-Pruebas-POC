const express = require('express')
const app = express()
const clientesService = require('../services/clientes')

app.get('/clientes/:email', async (req,res) => {
    
    try{
        let cliente = await clientesService.obtenerCliente(req.params.email)
        return res.status(cliente.id ? 200 : 400).json({
            cliente
        })
    }
    catch(err){
        return res.status(500).json({
            status: "Error: Hubo un error al buscar el cliente. Intentarlo más tarde.",
            reason: err.message
        })
    }
})

app.get('/clientes', async (req,res) => {
    
    try{
        let clientes = await clientesService.listarClientes()
        return res.status(clientes.length > 0 ? 200 : 400).json({
            clientes
        })
    }
    catch(err){
        return res.status(500).json({
            status: "Error: Hubo un error al buscar los cliente. Intentarlo más tarde.",
            reason: err.message
        })
    }
})

app.post('/clientes/:email', async(req,res)=>{
    try{
        console.log(req.params.email)
        let cliente = await clientesService.crearCliente(req.params.email)
        return res.status(201).json({
            cliente
        })
    }
    catch(err){
        return res.status(500).json({
            status: "Error: No se pudo crear al cliente. Intentarlo más tarde.",
            reason: err.message
        })
    }
})


module.exports = app