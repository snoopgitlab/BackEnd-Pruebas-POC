const express = require('express')
const app = express()
const testUserService = require('../services/testusers')

app.get('/crear_test_user', async(req,res) => {
    try {
        let response = await testUserService.crearTestUser()
        return res.status(200).json({
            response
        })
    }
    catch (err){
        return res.status(500).json({
            error: "Hubo un error. Intentarlo más tarde.",
            reason: err.message
        })
    }    
})

module.exports = app