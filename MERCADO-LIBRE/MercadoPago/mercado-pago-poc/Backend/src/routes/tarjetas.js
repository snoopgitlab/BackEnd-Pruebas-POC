const express = require('express')
const app = express()
const tarjetasService = require('../services/tarjetas')

app.post('/clientes/:email/tarjetas', async (req,res) => {
    
    const token = req.body.token

    try{
        let tarjeta = await tarjetasService.guardarTarjeta(token,req.params.email)
        return res.status(200).json({
            tarjeta
        })
    }
    catch(err){
        return res.status(500).json({
            status: "Hubo un error al guardar la tarjeta. Intentarlo más tarde.",
            reason: err.message
        })
    }
})

app.get('/clientes/:email/tarjetas', async (req,res) => {
    try{
        let tarjetas = await tarjetasService.listarTarjetas(req.params.email)
        return res.status(200).json({
            tarjetas
        })
    }
    catch(err){
        return res.status(500).json({
            status: "Hubo un error al consultar las tarjetas.",
            reason: err.message
        })
    }
})

module.exports = app