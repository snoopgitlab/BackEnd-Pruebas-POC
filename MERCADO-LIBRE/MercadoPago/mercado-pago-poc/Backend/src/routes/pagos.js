const express = require('express')
const app = express()
const pagosService = require('../services/pagos')

app.post('/pagos', async (req,res) => {
    
    let response = await pagosService.pagar(req.body, res)
    if(response.status == 201){
        return res.status(200).json({
            status: response.data.status,
            status_detail: response.data.status_detail,
            id: response.data.id
        });
    }
    else{
        return res.status(response.status).json({
            status: response.data.status,
            reason: response.data.cause
        })
    } 
})

module.exports = app