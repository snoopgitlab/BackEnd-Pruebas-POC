const express = require('express')

require("tls").DEFAULT_ECDH_CURVE = "auto"

const app = express()
const port = 3011

app.use(express.static((__dirname + '/public')))
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(require('./src/routes/pagos'))
app.use(require('./src/routes/clientes'))
app.use(require('./src/routes/tarjetas'))
app.use(require('./src/routes/testusers'))

app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${port}`)
})