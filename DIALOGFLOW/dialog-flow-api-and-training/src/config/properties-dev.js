module.exports = {
    EXPRESS_PORT: 8081,
    GDF_PROJECTID: "CompletarConIdProjectoGoogleCloud",
    GDF_LANGUAGE: "es",
    MYSQL_HOST: "localhost",
    MYSQL_PORT: "3306",
    MYSQL_USER: "CompletarConUser",
    MYSQL_PASSWORD: "CompletarConPassword",
    MYSQL_DATABASE: "dialog_flow"
}
