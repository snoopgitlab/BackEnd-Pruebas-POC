const express = require('express')
const bodyParser = require('body-parser')
const props = require('./config/properties-dev')
const MySQLConnector = require('./db/mysql')

const app = express()
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

// Definición de rutas
app.use('/api/v1/',require('./routes/conversation'))
app.use('/api/v1/',require('./routes/intents'))

// Inicio servidor
app.listen(props.EXPRESS_PORT, () => {
    console.log('El servidor se encuentra escuchando en el puerto: ' + props.EXPRESS_PORT);
});
