const mysql = require('mysql');
const props = require('../config/properties-dev')

class MySQLConnector {
    constructor(host,puerto,user,password,db){
        this.connect({
            MYSQL_HOST: host,
            MYSQL_PORT: puerto,
            MYSQL_USER: user,
            MYSQL_PASSWORD: password,
            MYSQL_DATABASE: db
        })
    }

    connect(params) {
        var connection = mysql.createConnection({
            host: params.MYSQL_HOST,
            port: params.MYSQL_PORT,
            user: params.MYSQL_USER,
            password: params.MYSQL_PASSWORD,
            database: params.MYSQL_DATABASE
        });
        console.log("Intentando crear conexión con MySQL...")
        connection.connect();
        console.log("Conexión creada correctamente")
        
        connection.query("SELECT 1 AS TEST")

        this.connection = connection
    }
    
    close() {
        console.log("Cerrando conexion....")
        this.connection.end(function(err){
            if (err) throw err
        });   
        console.log("Conexion cerrada exitosamente")
    }

    async executeQuery(query) {
        let resultSet
        this.connection.query(query,function(err, results, fields){
            if(err) throw err
            console.log(results)
            resultSet = results
        })

        console.log(resultSet)
        return resultSet
    }
    
}

module.exports = MySQLConnector
