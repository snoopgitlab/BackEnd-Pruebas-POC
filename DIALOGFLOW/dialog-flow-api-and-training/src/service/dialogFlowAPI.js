const dialogflow = require('@google-cloud/dialogflow');
const props = require('../config/properties-dev');
const MySQLConnector = require('../db/mysql');

// Instancio un conector de MySQL
let mysql = new MySQLConnector(
  props.MYSQL_HOST,
  props.MYSQL_PORT,
  props.MYSQL_USER,
  props.MYSQL_PASSWORD,
  props.MYSQL_DATABASE
)

const sessionClient = new dialogflow.SessionsClient();
const intentsClient = new dialogflow.IntentsClient();

const sendMessage = async (sessionId, message) => {

    let context
    let intentResponse
    let respuesta
    try {
        console.log(`Enviando mensaje: ${message}`);
        intentResponse = await detectIntent(props.GDF_PROJECTID,sessionId,message,context,props.GDF_LANGUAGE);
        console.log('Intent encontrado!');
        respuesta = intentResponse.queryResult.fulfillmentText
        // Se guarda el contexto para futuros mensajes (dura 20 min en Google igualmente)
        context = intentResponse.queryResult.outputContexts;
        intent = intentResponse.queryResult.intent.displayName
        query = "INSERT INTO messages (session_id,message,response,intent) " +
                `VALUES (${sessionId},"${message}","${respuesta}","${intent}");`
        console.log(query)
        mysql.executeQuery(query,function(err){
            if (err) throw err
            console.log("Mensaje guardado con éxito");
          })

        return respuesta;
    } 
    catch (error) {
        console.log(error);
        return "ERROR"
    }

}

const detectIntent = async (projectId,sessionId,query,contexts,languageCode) => {
  
    // The path to identify the agent that owns the created intent.
  const sessionPath = sessionClient.projectAgentSessionPath(
    projectId,
    sessionId
  );

  console.log(`SessionPath = ${sessionPath}`)

  // The text query request.
  const request = {
    session: sessionPath,
    queryInput: {
      text: {
        text: query,
        languageCode: languageCode,
      },
    },
  };

  if (contexts && contexts.length > 0) {
    request.queryParams = {
      contexts: contexts,
    };
  }

  const responses = await sessionClient.detectIntent(request);
  return responses[0];
}

const getIntents = async () => {

    const projectAgentPath = intentsClient.agentPath(props.GDF_PROJECTID);

    const request = {
      parent: projectAgentPath,
      intentView: 1
    };

    // Send the request for listing intents.
    const [response] = await intentsClient.listIntents(request);
    response.forEach(intent => {
      console.log('====================');
      console.log(`Intent name: ${intent.name}`);
      console.log(`Intent display name: ${intent.displayName}`);
    });
    
    return response.filter(s => s.displayName === "Reservar un turno")
}

const fit = async(intentName,trainingData) => {
  
  console.log(`Intent a buscar: ${intentName}`)
  let intents = await getIntents()
  let intentToUpdate = intents.filter(intent => intent.displayName === intentName)
  
  console.log(intentToUpdate)
  if(intentToUpdate.length == 0){
    return "El intent que se quiere entrenar no existe."
  }
  
  trainingData.training_phrases.forEach(p => {
    // Agregamos la frase de entrenamiento
    let phrase = {
      parts: [
        {
          text: p.text,
          entityType: "",
          alias: "",
          userDefined: false
        }
      ],
      name: "",
      type: "EXAMPLE",
      timesAddedCount: 0
    }
    intentToUpdate[0].trainingPhrases.push(phrase)
  })
  
  let update = await intentsClient.updateIntent({
      intent: intentToUpdate[0],
  }) 
  
  return update
}

module.exports = {
    sendMessage,
    getIntents,
    fit
}

