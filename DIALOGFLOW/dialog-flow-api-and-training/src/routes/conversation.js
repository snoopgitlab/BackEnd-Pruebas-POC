const express = require('express')
const df = require('../service/dialogFlowAPI')

// Instancio un router
const router = express.Router()

router.get('/conversation/init', async (req, res) => {
    res.status(200).json({
        status: "OK",
        sessionID: await generateNewSessionID()
    })
})

router.post('/conversation/:sessionId', async (req,res) => {
    
    let sessionId = req.params.sessionId
    let message = req.body.message
    // TODO Buscar ID de session activa y validar
    let respuesta = await df.sendMessage(sessionId,message)
    res.status(200).json({ 
        status: "OK",
        respuesta
    })
})

const generateNewSessionID = async () => {
    return Date.now()
}

module.exports = router