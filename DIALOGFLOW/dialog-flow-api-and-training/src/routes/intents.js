const express = require('express')
const df = require('../service/dialogFlowAPI')

// Instancio un router
const router = express.Router()

router.get('/intents', async (req, res) => {
    res.status(200).json({
        status: "OK",
        response: await df.getIntents()
    })
})

router.post('/intents/:intentName/learn', async (req, res) => {
    
    let intentName = req.params.intentName
    let response = await df.fit(intentName,req.body)
    res.status(200).json({
        status: "OK",
        response
    })
})

module.exports = router