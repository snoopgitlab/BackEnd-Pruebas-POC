const config = require('../config/config')
const axios = require('axios')

const MODULE_NAME = config.moduleName

const tiposHallazgo = [
    {
        name: "Excepción",
        value: "excepcion"
    },
    {
        name: "Riesgo",
        value: "riesgo"
    },
    {
        name: "Oportunidad de Mejora",
        value: "oportunidad_mejora"
    },
    {
        name: "No Conformidad",
        value: "no_conformidad"
    }
]

const prioridadesHallazgo = [
    {
        name: "alta",
        value: "P1"
    },
    {
        name: "media",
        value: "P2"
    },
    {
        name: "baja",
        value: "P3"
    },
    {
        name: "urgente",
        value: "urgente"
    }
]

const crearHallazgo = async(data) => {

    // Obtengo la sesión desde el archivo
    let auth = require('../config/credentials.json')
    const SESSION_ID = auth.sessionId
    console.log(`El id de sesión a enviar es ${SESSION_ID}`)

    //TODO Capturar de data (body) los valores a enviar
    const hallazgo = data.queryResult.parameters

    const nombre = hallazgo.nombre
    const tipoHallazgo = tiposHallazgo.find(tipo => tipo.name == hallazgo.tipo).value
    const prioridad = prioridadesHallazgo.find(prioridad => prioridad.name == hallazgo.prioridad).value
    const areaEmpresa = hallazgo.area_empresa
    const descripcion = hallazgo.descripcion
    
    const options = {
        method: 'POST',
        url: config.sgcEndpoint,
        params: {
            method: 'set_entry',
            input_type: 'JSON',
            response_type: 'JSON',
            rest_data: `{"session":"${SESSION_ID}",
                         "module_name":"${MODULE_NAME}",
                         "name_value_lists":[
                            {
                                "name":"name",
                                "value":"${nombre}"
                            },
                            {
                                "name":"tipo",
                                "value":"${tipoHallazgo}"
                            },
                            {
                                "name":"area_empresa",
                                "value":"${areaEmpresa}"
                            },
                            {
                                "name":"prioridad",
                                "value":"${prioridad}"
                            },
                            {
                                "name":"description",
                                "value":"${nombre}"
                            },
                            {
                                "name":"descripcion_c",
                                "value":"${descripcion}"
                            }
                        ]
                    }`
        }
    }
    
    try{
        const response = await axios(options)
        console.log(response)
        if(response.status == 200){
            if (response.data.name == 'Invalid Session ID')
                return "Hubo un error al registrar su hallazo. Por favor inténtelo más tarde."
            return "El hallazgo fue registrado con éxito. Gracias por utilizar el servicio!"
        }
        else
            return "Hubo un error al registrar su hallazo. Por favor inténtelo más tarde."
    }
    catch(error){
        console.log(error)
        return "Hubo un error al registrar su hallazo. Por favor inténtelo más tarde."
    }
}

module.exports = {
    crearHallazgo
}