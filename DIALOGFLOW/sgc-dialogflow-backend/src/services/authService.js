const config = require('../config/config')
const axios = require('axios')
const fs = require('fs')

const USERNAME = config.user
const PASSWORD = config.password
const APP = config.app

const generateSessionId = async() => {
    const options = {
        method: 'POST',
        url: config.sgcEndpoint,
        params: {
            method: 'login',
            input_type: 'JSON',
            response_type: 'JSON',
            rest_data: `{"user_auth":{"user_name":"${USERNAME}","password":"${PASSWORD}"},"application":"${APP}"}`
        }
    }
    try{
        const response = await axios(options)
        const sessionId = response.data.id
        console.log(`Id de sesión obtenido = ${sessionId}`)
        const data = {
            sessionId
        }
        fs.writeFile('src/config/credentials.json', JSON.stringify(data), (err) => {
            if (err) throw err
            console.log("Id de sesión guardado con éxito")
        })
        return {
            status: 'OK',
            sessionId
        }
    }
    catch(error){
        console.log(error)
        return {
            status: 'ERROR'
        }
    }
}


module.exports = {
    generateSessionId
}