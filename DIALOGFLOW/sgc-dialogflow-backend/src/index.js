const config = require('./config/config')
const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Cargo endpoints
app.use(require('./routes/hallazgos'))
app.use(require('./routes/auth'))

app.listen(config.port, () => {
    console.log(`Escuchando en el puerto ${config.port}`)
})