const express = require('express')
const app = express()
const hallazgosService = require('../services/hallazgosService')

app.post('/hallazgos', async(req,res) => {
    
    console.log(req.body)
    
    let response = await hallazgosService.crearHallazgo(req.body)
    
    return res.status(200).json(
        {
            fulfillmentMessages: [
                {
                    "text":{
                        "text":[response]
                    }
                }
            ]
        }
    )
})

module.exports = app