const express = require('express')
const app = express()
const authService = require('../services/authService')

app.get('/generateSessionId', async(req,res) => {
    
    let response = await authService.generateSessionId()

    return res.status(200).json(
        {
            status: response.status,
            session_id: response.sessionId
        }
    )
})

module.exports = app
