from flask import Flask, request, jsonify
import json
import requests
import datetime

# Se instancia Flask
app = Flask(__name__)

# Diccionario de precios de hamburguesas
hamburguesas = {'carne': 275, 'cordero': 300, 'pollo': 250, 'vegetariana': 225}
ultimo_precio = 0

# Variables para interactuar con la API del Backoffice
PEDIDOS_ENDPOINT = 'http://localhost:8100/api/pedidos'
AUTH_ENDPOINT = 'http://localhost:8100/api/authenticate'
    
nro_pedido = 0

def generateAuthToken():
    payload = {
        'password': 'admin',
        'rememberMe': True,
        'username': 'admin'
    }
    headers = {
        'Content-Type': 'application/json'
    }
    response = requests.post(AUTH_ENDPOINT, data=json.dumps(payload), headers=headers)
    data = response.json()
    token = data["id_token"]
    print("\nTOKEN GUARDADO =", token, '\n')
    return token 

# Genero el token cuando comienza el programa, dura 24 horas.
API_TOKEN = generateAuthToken()

def getCantidad(bebida): 
    return int(bebida["cantidad"])

def calculaPrecioHamburguesa(hamburguesa):
    
    precio_burga = 0
    
    # Calculamos el precio de la burga
    precio_burga += hamburguesas[hamburguesa['tipoHamburguesa']]
    
    # Le sumamos los toppings!
    # Valido existencia de toppings
    if 'toppings' in hamburguesa:
        toppings = hamburguesa['toppings']
        # Si es string, viene uno solo, sumamos 25
        if(isinstance(toppings,str)):
            precio_burga += 25
        # Si no string, es array, viene mas de uno y pedimos por su length
        else: 
            # 25 cada topping
            precio_burga += len(toppings)*25 
 
    return precio_burga

def calculaPrecioBebida(bebida): 
    # 80 cada bebida
    return int(bebida["cantidad"]) * 80

def calculaPrecio(pedido):
    
    precio = 0
    
    # Descomponemos el pedido para el calculo
    hamburguesas = pedido["ordenBurguer"]
    bebidas = pedido["orderBebida"]

    # Calculamos precio de las hamburguesas
    if len(hamburguesas) > 0:
        precio += sum(list(map(calculaPrecioHamburguesa,hamburguesas)))
    
    # Calculamos precio de bebidas
    if len(bebidas) > 0:
        precio += sum(list(map(calculaPrecioBebida,bebidas))) 

    return precio

def getNroPedido():
    global nro_pedido
    nro_pedido = nro_pedido + 1
    return nro_pedido

def getDescripcionBebida(bebida):
    return str(int(bebida["cantidad"])) + ' ' + bebida["bebida"]

def getDescripcionHamburguesa(hamburguesa):
    descripcion = ''

    #print("=========== HOLAAAA")
    # Agrego descripción del tipo de hamburguesa
    descripcion += str(int(hamburguesa['cantidad'])) + ' Hamburguesa de ' + hamburguesa['tipoHamburguesa']
    
    # Valido existencia de toppings
    if 'toppings' in hamburguesa:
        descripcion += ' con toppings: '
        toppings = hamburguesa['toppings']
        # Si es string, viene uno solo, lo concatenamos
        if(isinstance(toppings,str)):
            descripcion += toppings
        # Si no string, es array, viene mas de uno
        else: 
            descripcion += ', '.join(toppings)
    descripcion += "."

    return descripcion

def getDescripcion(pedido):
    
    descripcion = ''
    
    # Descomponemos el pedido
    hamburguesas = pedido['ordenBurguer']
    bebidas = pedido['orderBebida']

    # Agregamos hamburguesas
    if len(hamburguesas) != 0:
        descripcion += ' '.join(list(map(getDescripcionHamburguesa,hamburguesas)))
    
    # Agregamos bebidas
    if len(bebidas) != 0:
        descripcion += ' Bebidas: ' + ', '.join(list(map(getDescripcionBebida,bebidas))) + "."
    
    #print("==============", descripcion)
    return descripcion


def getDescTarjetaEfectivo(pedido):
    if pedido['tipoPago']=='tarjeta':
        return 'VISA 7314'
    else:
        return 'Efectivo'

def registrarPedido(pedido):

    # Armo el body
    payload = {
        'direccion': 'Julian Alvarez 751 8B',
        'fecha': str(datetime.datetime.now())[0:19],
        'nombreCliente': 'Héctor Ferraro',
        'nroPedido': getNroPedido(),
        'nroTelefono': '2215460595',
        'pago': getDescTarjetaEfectivo(pedido),
        'pedido': getDescripcion(pedido),
        'total': calculaPrecio(pedido)
    }

    # Preparo header con token
    headers = {
        'Authorization': 'Bearer ' + API_TOKEN,
        'Content-Type': 'application/json'    
    }
    # Ejecuto el POST
    response = requests.post(PEDIDOS_ENDPOINT, data=json.dumps(payload), headers=headers)
    #app.logger.info("Respuesta /pedidos \n", response.text)
    if response.status_code == 201:
        app.logger.info("el request al JHypster anduvo OOOK")
        return ["Tu pedido va en camino!. Que lo disfrutes!"]
    else:
        app.logger.info("Hubo un error al llamar al JHypster")
        app.logger.info("El response.code es: " + response.code)
        return ["Hubo un error al procesar el pedido. Inténtelo de nuevo más tarde."]

@app.route("/")
def home():
    return "Hello, World!"

@app.route("/query", methods = ['POST'])
def query():
    
    try:
        # Capturo el body
        body = json.dumps(request.json)
        app.logger.info('\nRequest recibido')
        app.logger.info(body)
        
        # Convierto el body a un objeto JSON
        data = json.loads(body)

        # Capturo el intent
        intent = data["queryResult"]["intent"]["displayName"] 
        app.logger.info("el intent es: " + intent)
        # Sólo manejamos dos opciones, por lo que un if resulta lo más simple para manejar este caso
        if intent == 'confirmarPedido_SI':
            respuesta = ['Son ' + str(calculaPrecio(data["queryResult"]["parameters"])) + ' pesos. ¿Quieres pagar con la tarjeta que termina en 7 3 1 4?']
        elif intent == 'envioDireccionRegistrada_SI':
            app.logger.info("voy a llamar al JHypster")
            respuesta = registrarPedido(data["queryResult"]["parameters"])
        else: 
            return jsonify(
                fulfillmentMessages = [
                    {"text":{"text":["No entiendo que debo hacer."]}}]
                )

        return jsonify(
            fulfillmentMessages = [
                {"text":{"text":respuesta}}]
            )

    except Exception as e:
        app.logger.info("la excepcion es:")
        app.logger.info(e)
        return jsonify(
            fulfillmentMessages = [
              {"text":{"text":["Explotó"]}}]
            )

if __name__ == "__main__":
    app.run(debug=True)
