/**
 * View Models used by Spring MVC REST controllers.
 */
package com.snoop.voice.web.rest.vm;
