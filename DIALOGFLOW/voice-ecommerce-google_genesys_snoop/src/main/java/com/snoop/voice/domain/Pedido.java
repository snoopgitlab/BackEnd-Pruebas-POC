package com.snoop.voice.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Pedido.
 */
@Entity
@Table(name = "pedido")
public class Pedido implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nro_pedido", nullable = false)
    private Integer nroPedido;

    @Column(name = "fecha")
    private String fecha;

    @Column(name = "nombre_cliente")
    private String nombreCliente;

    @Column(name = "nro_telefono")
    private String nroTelefono;

    @Column(name = "pedido")
    private String pedido;

    @Column(name = "total")
    private Float total;

    @Column(name = "pago")
    private String pago;

    @Column(name = "direccion")
    private String direccion;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNroPedido() {
        return nroPedido;
    }

    public Pedido nroPedido(Integer nroPedido) {
        this.nroPedido = nroPedido;
        return this;
    }

    public void setNroPedido(Integer nroPedido) {
        this.nroPedido = nroPedido;
    }

    public String getFecha() {
        return fecha;
    }

    public Pedido fecha(String fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public Pedido nombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
        return this;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public Pedido nroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
        return this;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }

    public String getPedido() {
        return pedido;
    }

    public Pedido pedido(String pedido) {
        this.pedido = pedido;
        return this;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public Float getTotal() {
        return total;
    }

    public Pedido total(Float total) {
        this.total = total;
        return this;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getPago() {
        return pago;
    }

    public Pedido pago(String pago) {
        this.pago = pago;
        return this;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getDireccion() {
        return direccion;
    }

    public Pedido direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pedido)) {
            return false;
        }
        return id != null && id.equals(((Pedido) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Pedido{" +
            "id=" + getId() +
            ", nroPedido=" + getNroPedido() +
            ", fecha='" + getFecha() + "'" +
            ", nombreCliente='" + getNombreCliente() + "'" +
            ", nroTelefono='" + getNroTelefono() + "'" +
            ", pedido='" + getPedido() + "'" +
            ", total=" + getTotal() +
            ", pago='" + getPago() + "'" +
            ", direccion='" + getDireccion() + "'" +
            "}";
    }
}
