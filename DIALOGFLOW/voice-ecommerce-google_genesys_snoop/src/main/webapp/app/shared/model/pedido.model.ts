export interface IPedido {
  id?: number;
  nroPedido?: number;
  fecha?: string;
  nombreCliente?: string;
  nroTelefono?: string;
  pedido?: string;
  total?: number;
  pago?: string;
  direccion?: string;
}

export const defaultValue: Readonly<IPedido> = {};
