import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './pedido.reducer';
import { IPedido } from 'app/shared/model/pedido.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPedidoProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const Pedido = (props: IPedidoProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { pedidoList, match, loading } = props;
  return (
    <div>
      <h2 id="pedido-heading">
        Pedidos
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Pedido
        </Link>
      </h2>
      <div className="table-responsive">
        {pedidoList && pedidoList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nro Pedido</th>
                <th>Fecha</th>
                <th>Nombre Cliente</th>
                <th>Nro Telefono</th>
                <th>Pedido</th>
                <th>Total</th>
                <th>Pago</th>
                <th>Direccion</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {pedidoList.map((pedido, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${pedido.id}`} color="link" size="sm">
                      {pedido.id}
                    </Button>
                  </td>
                  <td>{pedido.nroPedido}</td>
                  <td>{pedido.fecha}</td>
                  <td>{pedido.nombreCliente}</td>
                  <td>{pedido.nroTelefono}</td>
                  <td>{pedido.pedido}</td>
                  <td>{pedido.total}</td>
                  <td>{pedido.pago}</td>
                  <td>{pedido.direccion}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${pedido.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${pedido.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${pedido.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Pedidos found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ pedido }: IRootState) => ({
  pedidoList: pedido.entities,
  loading: pedido.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Pedido);
