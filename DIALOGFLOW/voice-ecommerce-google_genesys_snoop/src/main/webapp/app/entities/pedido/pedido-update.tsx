import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './pedido.reducer';
import { IPedido } from 'app/shared/model/pedido.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPedidoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PedidoUpdate = (props: IPedidoUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { pedidoEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/pedido');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...pedidoEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="eCommerceApp.pedido.home.createOrEditLabel">Create or edit a Pedido</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : pedidoEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="pedido-id">ID</Label>
                  <AvInput id="pedido-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="nroPedidoLabel" for="pedido-nroPedido">
                  Nro Pedido
                </Label>
                <AvField
                  id="pedido-nroPedido"
                  type="string"
                  className="form-control"
                  name="nroPedido"
                  validate={{
                    required: { value: true, errorMessage: 'This field is required.' },
                    number: { value: true, errorMessage: 'This field should be a number.' },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="fechaLabel" for="pedido-fecha">
                  Fecha
                </Label>
                <AvField id="pedido-fecha" type="text" name="fecha" />
              </AvGroup>
              <AvGroup>
                <Label id="nombreClienteLabel" for="pedido-nombreCliente">
                  Nombre Cliente
                </Label>
                <AvField id="pedido-nombreCliente" type="text" name="nombreCliente" />
              </AvGroup>
              <AvGroup>
                <Label id="nroTelefonoLabel" for="pedido-nroTelefono">
                  Nro Telefono
                </Label>
                <AvField id="pedido-nroTelefono" type="text" name="nroTelefono" />
              </AvGroup>
              <AvGroup>
                <Label id="pedidoLabel" for="pedido-pedido">
                  Pedido
                </Label>
                <AvField id="pedido-pedido" type="text" name="pedido" />
              </AvGroup>
              <AvGroup>
                <Label id="totalLabel" for="pedido-total">
                  Total
                </Label>
                <AvField id="pedido-total" type="string" className="form-control" name="total" />
              </AvGroup>
              <AvGroup>
                <Label id="pagoLabel" for="pedido-pago">
                  Pago
                </Label>
                <AvField id="pedido-pago" type="text" name="pago" />
              </AvGroup>
              <AvGroup>
                <Label id="direccionLabel" for="pedido-direccion">
                  Direccion
                </Label>
                <AvField id="pedido-direccion" type="text" name="direccion" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/pedido" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  pedidoEntity: storeState.pedido.entity,
  loading: storeState.pedido.loading,
  updating: storeState.pedido.updating,
  updateSuccess: storeState.pedido.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PedidoUpdate);
