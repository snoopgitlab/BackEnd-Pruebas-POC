import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pedido.reducer';
import { IPedido } from 'app/shared/model/pedido.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPedidoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const PedidoDetail = (props: IPedidoDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { pedidoEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Pedido [<b>{pedidoEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="nroPedido">Nro Pedido</span>
          </dt>
          <dd>{pedidoEntity.nroPedido}</dd>
          <dt>
            <span id="fecha">Fecha</span>
          </dt>
          <dd>{pedidoEntity.fecha}</dd>
          <dt>
            <span id="nombreCliente">Nombre Cliente</span>
          </dt>
          <dd>{pedidoEntity.nombreCliente}</dd>
          <dt>
            <span id="nroTelefono">Nro Telefono</span>
          </dt>
          <dd>{pedidoEntity.nroTelefono}</dd>
          <dt>
            <span id="pedido">Pedido</span>
          </dt>
          <dd>{pedidoEntity.pedido}</dd>
          <dt>
            <span id="total">Total</span>
          </dt>
          <dd>{pedidoEntity.total}</dd>
          <dt>
            <span id="pago">Pago</span>
          </dt>
          <dd>{pedidoEntity.pago}</dd>
          <dt>
            <span id="direccion">Direccion</span>
          </dt>
          <dd>{pedidoEntity.direccion}</dd>
        </dl>
        <Button tag={Link} to="/pedido" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/pedido/${pedidoEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ pedido }: IRootState) => ({
  pedidoEntity: pedido.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(PedidoDetail);
