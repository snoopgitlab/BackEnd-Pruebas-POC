package com.snoop.voice.web.rest;

import com.snoop.voice.ECommerceApp;
import com.snoop.voice.domain.Pedido;
import com.snoop.voice.repository.PedidoRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PedidoResource} REST controller.
 */
@SpringBootTest(classes = ECommerceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PedidoResourceIT {

    private static final Integer DEFAULT_NRO_PEDIDO = 1;
    private static final Integer UPDATED_NRO_PEDIDO = 2;

    private static final String DEFAULT_FECHA = "AAAAAAAAAA";
    private static final String UPDATED_FECHA = "BBBBBBBBBB";

    private static final String DEFAULT_NOMBRE_CLIENTE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_CLIENTE = "BBBBBBBBBB";

    private static final String DEFAULT_NRO_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_NRO_TELEFONO = "BBBBBBBBBB";

    private static final String DEFAULT_PEDIDO = "AAAAAAAAAA";
    private static final String UPDATED_PEDIDO = "BBBBBBBBBB";

    private static final Float DEFAULT_TOTAL = 1F;
    private static final Float UPDATED_TOTAL = 2F;

    private static final String DEFAULT_PAGO = "AAAAAAAAAA";
    private static final String UPDATED_PAGO = "BBBBBBBBBB";

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPedidoMockMvc;

    private Pedido pedido;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pedido createEntity(EntityManager em) {
        Pedido pedido = new Pedido()
            .nroPedido(DEFAULT_NRO_PEDIDO)
            .fecha(DEFAULT_FECHA)
            .nombreCliente(DEFAULT_NOMBRE_CLIENTE)
            .nroTelefono(DEFAULT_NRO_TELEFONO)
            .pedido(DEFAULT_PEDIDO)
            .total(DEFAULT_TOTAL)
            .pago(DEFAULT_PAGO)
            .direccion(DEFAULT_DIRECCION);
        return pedido;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pedido createUpdatedEntity(EntityManager em) {
        Pedido pedido = new Pedido()
            .nroPedido(UPDATED_NRO_PEDIDO)
            .fecha(UPDATED_FECHA)
            .nombreCliente(UPDATED_NOMBRE_CLIENTE)
            .nroTelefono(UPDATED_NRO_TELEFONO)
            .pedido(UPDATED_PEDIDO)
            .total(UPDATED_TOTAL)
            .pago(UPDATED_PAGO)
            .direccion(UPDATED_DIRECCION);
        return pedido;
    }

    @BeforeEach
    public void initTest() {
        pedido = createEntity(em);
    }

    @Test
    @Transactional
    public void createPedido() throws Exception {
        int databaseSizeBeforeCreate = pedidoRepository.findAll().size();
        // Create the Pedido
        restPedidoMockMvc.perform(post("/api/pedidos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pedido)))
            .andExpect(status().isCreated());

        // Validate the Pedido in the database
        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeCreate + 1);
        Pedido testPedido = pedidoList.get(pedidoList.size() - 1);
        assertThat(testPedido.getNroPedido()).isEqualTo(DEFAULT_NRO_PEDIDO);
        assertThat(testPedido.getFecha()).isEqualTo(DEFAULT_FECHA);
        assertThat(testPedido.getNombreCliente()).isEqualTo(DEFAULT_NOMBRE_CLIENTE);
        assertThat(testPedido.getNroTelefono()).isEqualTo(DEFAULT_NRO_TELEFONO);
        assertThat(testPedido.getPedido()).isEqualTo(DEFAULT_PEDIDO);
        assertThat(testPedido.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testPedido.getPago()).isEqualTo(DEFAULT_PAGO);
        assertThat(testPedido.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
    }

    @Test
    @Transactional
    public void createPedidoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pedidoRepository.findAll().size();

        // Create the Pedido with an existing ID
        pedido.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPedidoMockMvc.perform(post("/api/pedidos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pedido)))
            .andExpect(status().isBadRequest());

        // Validate the Pedido in the database
        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNroPedidoIsRequired() throws Exception {
        int databaseSizeBeforeTest = pedidoRepository.findAll().size();
        // set the field null
        pedido.setNroPedido(null);

        // Create the Pedido, which fails.


        restPedidoMockMvc.perform(post("/api/pedidos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pedido)))
            .andExpect(status().isBadRequest());

        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPedidos() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        // Get all the pedidoList
        restPedidoMockMvc.perform(get("/api/pedidos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pedido.getId().intValue())))
            .andExpect(jsonPath("$.[*].nroPedido").value(hasItem(DEFAULT_NRO_PEDIDO)))
            .andExpect(jsonPath("$.[*].fecha").value(hasItem(DEFAULT_FECHA)))
            .andExpect(jsonPath("$.[*].nombreCliente").value(hasItem(DEFAULT_NOMBRE_CLIENTE)))
            .andExpect(jsonPath("$.[*].nroTelefono").value(hasItem(DEFAULT_NRO_TELEFONO)))
            .andExpect(jsonPath("$.[*].pedido").value(hasItem(DEFAULT_PEDIDO)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].pago").value(hasItem(DEFAULT_PAGO)))
            .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION)));
    }
    
    @Test
    @Transactional
    public void getPedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        // Get the pedido
        restPedidoMockMvc.perform(get("/api/pedidos/{id}", pedido.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(pedido.getId().intValue()))
            .andExpect(jsonPath("$.nroPedido").value(DEFAULT_NRO_PEDIDO))
            .andExpect(jsonPath("$.fecha").value(DEFAULT_FECHA))
            .andExpect(jsonPath("$.nombreCliente").value(DEFAULT_NOMBRE_CLIENTE))
            .andExpect(jsonPath("$.nroTelefono").value(DEFAULT_NRO_TELEFONO))
            .andExpect(jsonPath("$.pedido").value(DEFAULT_PEDIDO))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.pago").value(DEFAULT_PAGO))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION));
    }
    @Test
    @Transactional
    public void getNonExistingPedido() throws Exception {
        // Get the pedido
        restPedidoMockMvc.perform(get("/api/pedidos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        int databaseSizeBeforeUpdate = pedidoRepository.findAll().size();

        // Update the pedido
        Pedido updatedPedido = pedidoRepository.findById(pedido.getId()).get();
        // Disconnect from session so that the updates on updatedPedido are not directly saved in db
        em.detach(updatedPedido);
        updatedPedido
            .nroPedido(UPDATED_NRO_PEDIDO)
            .fecha(UPDATED_FECHA)
            .nombreCliente(UPDATED_NOMBRE_CLIENTE)
            .nroTelefono(UPDATED_NRO_TELEFONO)
            .pedido(UPDATED_PEDIDO)
            .total(UPDATED_TOTAL)
            .pago(UPDATED_PAGO)
            .direccion(UPDATED_DIRECCION);

        restPedidoMockMvc.perform(put("/api/pedidos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPedido)))
            .andExpect(status().isOk());

        // Validate the Pedido in the database
        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeUpdate);
        Pedido testPedido = pedidoList.get(pedidoList.size() - 1);
        assertThat(testPedido.getNroPedido()).isEqualTo(UPDATED_NRO_PEDIDO);
        assertThat(testPedido.getFecha()).isEqualTo(UPDATED_FECHA);
        assertThat(testPedido.getNombreCliente()).isEqualTo(UPDATED_NOMBRE_CLIENTE);
        assertThat(testPedido.getNroTelefono()).isEqualTo(UPDATED_NRO_TELEFONO);
        assertThat(testPedido.getPedido()).isEqualTo(UPDATED_PEDIDO);
        assertThat(testPedido.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testPedido.getPago()).isEqualTo(UPDATED_PAGO);
        assertThat(testPedido.getDireccion()).isEqualTo(UPDATED_DIRECCION);
    }

    @Test
    @Transactional
    public void updateNonExistingPedido() throws Exception {
        int databaseSizeBeforeUpdate = pedidoRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPedidoMockMvc.perform(put("/api/pedidos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(pedido)))
            .andExpect(status().isBadRequest());

        // Validate the Pedido in the database
        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePedido() throws Exception {
        // Initialize the database
        pedidoRepository.saveAndFlush(pedido);

        int databaseSizeBeforeDelete = pedidoRepository.findAll().size();

        // Delete the pedido
        restPedidoMockMvc.perform(delete("/api/pedidos/{id}", pedido.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pedido> pedidoList = pedidoRepository.findAll();
        assertThat(pedidoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
