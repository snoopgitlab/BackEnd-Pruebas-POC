// Inicializo configuración del servidor en la primer línea.
require('./config/config')

const express = require('express')
const bodyParser = require('body-parser')
const path = require('path') // Librería que nos permite armar paths (viene con Node)

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Habilitar la carpeta public
app.use(express.static(path.resolve(__dirname,  "../public")))

// Cargo endpoints
app.use(require('./routes/googleauth'))
app.use(require('./routes/events'))

app.listen(process.env.PORT, () => {
    console.log('Escuchando en el puerto', process.env.PORT)
})