const express = require('express')
const fs = require('fs')
const googleAPI = require('../services/eventsService')
const { google } = require('googleapis')
const app = express()

// Usar estos dos métodos para obtener un nuevo CODE y renovar el ACCESS_TOKEN (Bearer)
// Crea una URL para autenticarse manualmente desde la WEB y devolver el ACCESS_CODE
app.get('/google/authUrl', async (req, res) => {
    const oauth2Client = new google.auth.OAuth2(process.env.CLIENT_ID,process.env.CLIENT_SECRET,process.env.REDIRECT_URL);

    const url = oauth2Client.generateAuthUrl({
        // 'online' (default) or 'offline' (gets refresh_token)
        access_type: 'offline',

        // Se le pasa el alcance que va a tener el token
        scope: [
            'https://www.googleapis.com/auth/calendar'
        ]
    })
    return res.status(200).json({ 
        url
    })
})

// Guarda el código de autenticación, para pedir el token luego
app.get('/google/oauth2callback', async (req,res) => {

    const authCode = req.query.code
    let data = require('../config/authorization.json')
    data.access_code = authCode

    fs.writeFile('server/config/authorization.json', JSON.stringify(data), (err) => {
        if (err) throw err
        console.log("Código de acceso guardado con éxito")
        console.log(authCode)
    })

    return res.status(200).json({ 
        authCode
    })
})

// Generar el token por primera vez a partir de un CODE
app.get('/google/generarToken', async (req,res) => {
    
    // Obtengo código
    let data = require('../config/authorization.json')
    const code = data.access_code

    let response = await googleAPI.generarToken(code)
    if(response.status === 200){
        let token = response.data.access_token
        data.access_token = token
        fs.writeFile('server/config/authorization.json', JSON.stringify(data), (err) => {
            if (err) throw err
            console.log("Token de acceso guardado con éxito")
            console.log(token)
        })
        return res.status(200).json({ 
            token
        }) 
    }
    else{
        return res.status(500).json({
            response: response.data
        })
    }
    
})

// Renovar el token a partir del REFRESH TOKEN
app.get('/google/renovarToken', async (req,res) => {
    
    // Obtengo código
    let data = require('../config/authorization.json')
    const refreshToken = data.refresh_token

    let response = await googleAPI.renovarToken(refreshToken)
    if(response.status === 200){
        let token = response.data.access_token
        data.access_token = token
        fs.writeFile('server/config/authorization.json', JSON.stringify(data), (err) => {
            if (err) throw err
            console.log("Token de acceso guardado con éxito")
            console.log(token)
        })
        return res.status(200).json({ 
            token
        }) 
    }
    else{
        return res.status(500).json({
            response: response.data
        })
    }
    
})

module.exports = app