const express = require('express')
const app = express()

// Obtengo el servicio de Google
const google = require('../services/eventsService')

// Obtengo los últimos 10 eventos
app.get('/events', async (req, res) => {
    
    let response = await google.obtenerEventos()
    if(response.status === 200)
        return res.status(200).json({ 
            events: response.data.items.map(e => { 
            return {
                    nombre: e.summary,
                    descripcion: e.description,
                    creador: e.creator.displayName,
                    organizador: e.organizer.displayName,
                    fecha: e.start.date,
                    meetLink: e.hangoutLink
                }
            })
        })
    if(response.status === 401 || 400)
        return res.status(200).json({ 
            message: "Hubo un problema al autenticarse con la API de Google",
            error: response.data
    })
})

// Creo un evento
app.post('/events', async (req, res) => {
    
    let response = await google.crearEvento(req.body)
    if(response.status === 200)
        return res.status(200).json({ 
            message: "Se ha creado con éxito el evento"
        })
    else
        return res.status(200).json({ 
            message: response.data
        })
})

// Chequea si hay lugar en el calendario para asignar un evento
app.get('/reservation', async (req,res) => {
    
    let fecha = req.query.fecha
    let hora = req.query.hora
    try{
        let disponible = await google.checkDisponibilidad(fecha,hora)
        
        return res.status(200).json({
            message: disponible ? "El horario está disponible." : "El lugar está ocupado"
        })
    }
    catch(e)
    {
        return res.status(200).json({
            message: e.message
        })
    }

})
module.exports = app