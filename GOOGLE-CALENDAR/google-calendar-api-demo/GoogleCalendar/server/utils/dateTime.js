// Funciones de fecha y hora
const normalizarHora = (hora) => {
    return hora.length === 5 ? hora : ("0" + hora)
}

// Verifica si una fecha está incluida dentro de un rango de fechas
const estaIncluidaEnRango = (fecha, fechas) => {

    let incluidas = fechas.filter(f => f.start < fecha.getTime() && fecha.getTime() < f.end)
    console.log(incluidas.length > 0)
    return incluidas.length > 0
}

// Convierte una lista de eventos de Google a una lista de fechas y horas de comienzo y fin
const obtenerFechas = (eventos) => {
   
    return eventos.map(e => {
        if(e.start.dateTime && e.start.dateTime)
        {   
            let start = new Date(e.start.dateTime.substring(0,e.start.dateTime.length-6))
            let end = new Date(e.end.dateTime.substring(0,e.end.dateTime.length-6))
            return {
                start: start.setHours(start.getHours()),
                end: end.setHours(end.getHours())
            }
        }
        else
            return {
                start: new Date(e.start.date + "T00:00:00").getTime(),
                end: new Date(e.end.date + "T00:00:00").getTime()
            }
    })
} 

module.exports = {
    normalizarHora,
    estaIncluidaEnRango,
    obtenerFechas
}