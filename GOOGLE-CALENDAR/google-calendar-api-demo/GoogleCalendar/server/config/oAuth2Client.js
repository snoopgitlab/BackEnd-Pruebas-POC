const { google } = require('googleapis')
const fs = require('fs')
const TOKEN_PATH = 'server/config/token.json'
const axios = require('axios')

const createNewToken = async () => {

    let token 
    
    // Armo request
    const options = {
		method: 'POST',
		url: "https://oauth2.googleapis.com/token",
        body:{
            code: process.env.ACCESS_CODE,
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            redirect_uri: process.env.REDIRECT_URL,
            grant_type: 'authorization_code'
        }
    }
    await axios(options)
        .then(resp => {
            console.log(resp.data)
            token = resp
        })
        .catch(error => {
            console.log(error)
            token = error.response
        })
    return token
}

module.exports = {
    createNewToken
}