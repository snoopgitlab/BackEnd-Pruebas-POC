const axios = require('axios')
const dateUtils = require('../utils/dateTime')

const EVENTS_ENDPOINT = "https://www.googleapis.com/calendar/v3/calendars/primary/events"
const AUTH_ENDPOINT = "https://oauth2.googleapis.com/token"
const TIME_ZONE = "America/Argentina/Buenos_Aires"
const AUTH_PATH = '../config/authorization.json'


const obtenerEventos = async () => {

    // Obtengo código
    let data = require('../config/authorization.json')
    const token = data.access_token
    
    // Si se obtuvo el token se sigue
    console.log("====== Token a enviar: ", token)
    
    // Armo request
    const options = {
		method: 'GET',
		url: "https://www.googleapis.com/calendar/v3/calendars/primary/events?timeMin=2020-08-20T17%3A47%3A42.516Z&maxResults=10&singleEvents=true&orderBy=startTime",
		headers: {
			'Authorization': 'Bearer ' + token
        }
    }
    await axios(options)
        .then(resp => {
            console.log(resp.data)
            response = resp
        })
        .catch(error => {
            console.log(error)
            response = error.response
        })
    return response
}

const crearEvento = async(evento) => {
    
    let response

    // Obtengo token
    let data = require(AUTH_PATH)
    const token = data.access_token
    
    // Obtengo los atributos del evento
    const {titulo, descripcion, fecha_inicio, hora_inicio, fecha_fin, hora_fin, lugar, invitados} = evento
   
    // Armo request
    const options = {
		method: 'POST',
		url: "https://www.googleapis.com/calendar/v3/calendars/primary/events",
		headers: {
			'Authorization': 'Bearer ' + token
        },
        data: {
            summary: titulo,
            location: lugar,
            description: descripcion,
            start: {
                "dateTime": fecha_inicio + "T" + hora_inicio + ":00",
                "timeZone": TIME_ZONE
            },
            end: {
                "dateTime": fecha_fin + "T" + hora_fin + ":00",
                "timeZone": TIME_ZONE
            },
            "attendees": invitados.map(inv => { return { "email": inv } })
        }
    }
    await axios(options)
        .then(resp => {
            console.log(resp.data)
            response = resp
        })
        .catch(error => {
            console.log(error)
            response = error.response
        })
    return response
}

const checkDisponibilidad = async(fecha,hora) => {
    
    // Obtengo código
    let data = require(AUTH_PATH)
    const token = data.access_token
    
    // Vamos a pedir todos los eventos del día.
    const inicio = fecha + "T00:00:00-03:00"
    const fin = fecha + "T23:59:59-03:00"

    // Armo request
    const options = {
		method: 'GET',
        url: `${EVENTS_ENDPOINT}?timeMin=${inicio}&timeMax=${fin}&maxResults=250&singleEvents=true&timeZone=${TIME_ZONE}`,
		headers: {
			'Authorization': 'Bearer ' + token
        }
    }

    await axios(options)
        .then(resp => { 
            let fechas = dateUtils.obtenerFechas(resp.data.items) 
            let solicitada = new Date(fecha + "T" + dateUtils.normalizarHora(hora) + ":00")
            let libre = !dateUtils.estaIncluidaEnRango(solicitada, fechas)     
            response = libre
        })
        .catch(error => {
            console.log(error)
            throw new Error("Hubo un error, inténtelo más tarde")
        })
    return response
}

const generarToken = async(code) => {

    // Armo request
    const options = {
		method: 'POST',
		url: AUTH_ENDPOINT,
		data: {
            code: code,
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            redirect_uri: process.env.REDIRECT_URL,
            grant_type: 'authorization_code'
        }
    }
    await axios(options)
        .then(resp => {
            console.log(resp.data)
            response = resp
        })
        .catch(error => {
            console.log(error)
            response = error.response
        })
    return response


}

const renovarToken = async(refreshToken) => {

    // Armo request
    const options = {
		method: 'POST',
		url: AUTH_ENDPOINT,
		data: {
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET,
            refresh_token: refreshToken,
            grant_type: 'refresh_token'
        }
    }
    await axios(options)
        .then(resp => {
            console.log(resp.data)
            response = resp
        })
        .catch(error => {
            console.log(error)
            response = error.response
        })
    return response
}

module.exports = {
    obtenerEventos,
    crearEvento,
    generarToken,
    renovarToken,
    checkDisponibilidad
}